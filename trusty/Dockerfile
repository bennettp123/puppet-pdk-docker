FROM library/ubuntu:14.04

# download link for PDK
# see https://puppet.com/download-puppet-development-kit
ARG PDK_URL=https://pm.puppetlabs.com/cgi-bin/pdk_download.cgi?dist=ubuntu&rel=14.04&arch=amd64&ver=latest

# build dependencies are required to install PDK
ARG BUILD_PACKAGES=curl

# install build dependencies
RUN apt-get update \
 && apt-get install -y \
        locales \
        $BUILD_PACKAGES \

# generate a UTF-8 locale
 && locale-gen en_US.UTF-8 \

# download and install PDK
 && curl -L "$PDK_URL" > pdk.deb \
 && dpkg -i pdk.deb \

# install any missing dependencies
 && apt-get install -f \

# clean up afterwards
# https://www.dajobe.org/blog/2015/04/18/making-debian-docker-images-smaller/
 && rm -f pdk.deb \
 && apt-get remove --purge -y \
        $BUILD_PACKAGES \
        $(apt-mark showauto) \
 && rm -rf /var/lib/apt/lists/*

ENV PATH=/opt/puppetlabs/bin:${PATH}

# US-ASCII is the default locale in the 14.04 image
# but the JSON spec enforces UTF-8
# hardcode UTF-8 here
ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8

CMD ["/opt/puppetlabs/bin/pdk"]

