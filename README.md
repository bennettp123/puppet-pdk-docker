# puppet-pdk-docker 

This project defines docker images for running PDK.

# Quickstart

```
docker login gitlab.i8rs.com:4567
docker pull gitlab.i8rs.com:4567/bennettp123/puppet-pdk-docker:latest
docker run --rm -it gitlab.i8rs.com:4567/bennettp123/puppet-pdk-docker:latest pdk --help
```

![screenshot](assets/screenshot.png)

